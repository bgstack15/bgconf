File: /usr/share/bgconf/docs/README.md
Package: bgconf
Author: bgstack15
Startdate: 2017-04-09
Title: Readme file for bgconf
Purpose: All packages should come with a readme
Usage: Read it.
Reference: README.txt
Improve:
Document: Below this line

### WELCOME
Bgconf includes my standard configs and a python script that deploys them.
The confs are provided in /usr/share/bgconf/confs/ and the definitions are in /usr/share/bgconf/bgconf.conf
After installation of the rpm, execute as the preferred user:
sudo bgconf.py

### NOTES

### REFERENCE
kerberos https://bgstack15.wordpress.com/2017/12/11/fedora-27-ssh-and-default-kerberos-config/

### CHANGELOG

2017-04-09 B Stack <bgstack15@gmail.com> 0.1-1
- Wrote bgconf.conf

2017-04-22 B Stack <bgstack15@gmail.com> 0.1-2
- added vlc

2017-04-23 B Stack <bgstack15@gmail.com> 0.1-3
- Added xscreensaver
- Added puddletag
- Added plank not run on start
- Added bgstack15-red-k25 theme to ~/.themes directory
- Added cinnamon configs

2017-05-03 B Stack <bgstack15@gmail.com> 0.1-4
- Added git
- Added firefox extensions: password exporter, secure login (webextension), saved password editor

2017-05-28 B Stack <bgstack15@gmail.com> 0.1-5
- Added ott template file
- Updated fstab for Mersey network

2017-05-31 B Stack <bgstack15@gmail.com> 0.1-6
- Fixed the ./pack error where it makes a "cd" directory.
- Only runs dconf if it exists in cinnamon.
- Added GSSAPIDelegateCredentials yes to ssh_config.
- Added deb packaging.

2017-06-04 B Stack <bgstack15@gmail.com> 0.1-7
- Fixed the absence of the .git in the confs directory.

2017-06-12 B Stack <bgstack15@gmail.com> 0.1-8
- Added conf for dash fix

2017-08-22 B Stack <bgstack15@gmail.com> 0.1-9
- Rearranged directories to match latest bgscripts spec
- Moved desktop theme to own 'application' heading
- Added numix-circle icon theme to desktop-theme
- Added gnome-terminal settings to cinnamon dconf
- Added vimrc
- Added nemo settings to cinnamon dconf

2017-09-17 B Stack <bgstack15@gmail.com> 0.1-10
- Fixed git config again
- Added xfce.sh script
- Added ~/.bcrc
- Abstracted out the dconf and xfconf commands for easier updating in the future

2017-10-14 B Stack <bgstack15@gmail.com> 0.1-11
- Updated and fixed dconf.sh and xfconf.sh to find the running DE a better way

* Sun Nov 19 2017 B Stack <bgstack15@gmail.com> 0.1-12
- Updated and fixed dconf.sh and xfconf.sh to find the running DE a better way again
- Fixed script calls to updateval.py for bgscripts 1.3-0
- Updated vimrc with tabstop info
- Updated Firefox for version 57 extensions, which requires some custom logic.

* Sun Dec 10 2017 B Stack <bgstack15@gmail.com> 0.1-13
- Add copyq
- Add display-manager lightdm
- Add my ssh public key
- Add FF57+ Flash Block (Plus)
- Fix Thunar settings
- Fix vimrc scrolloff=0
- Fix and cleanup bgstack15-red-k25 theme
-  xfwm4: fix window borders

* Sun Jan 28 2018 B Stack <bgstack15@gmail.com> 0.1-14
- firefox: minor config changes
- vimrc: remove powerline plugin contents
- Add lightdm-gtk-greeter
- Add screenrc
- Add kerberos config
- Update scite fonts to be fixed-width
- Update cinnamon dconf window tiling snap osd and hud off

* Sat Apr 28 2018 B Stack <bgstack15@gmail.com> 0.1-15
- display-manager: apply config change instead of being verbose
- Add my grub configs (no rhgb or quiet)
- kerberos: fix permissions

* Sun Jul 15 2018 B Stack <bgstack15@gmail.com> 0.1-16
- ssh: fix owner of ~/.ssh directory and file
- xfce: add window manager keyboard shortcuts

* Fri Sep 28 2018 B Stack <bgstack15@gmail.com> 0.1-17
- desktop-theme: add auto-mnemonics=1
- display-manager: actually work and apply changes
- firefox:
-  add ntlm-auth.trusted-uris
-  add ipa cert to trusted store
-  fix version check for firefox-esr package on devuan
- git config: add diff.color=auto
- ssh:
-  fix authorized_keys ownership
-  add missing quote to script so it actually operates
- xfce4: terminal color scheme

* Thu Feb 21 2019 B Stack <bgstack15@gmail.com> - 0.1.18-0
- rearrange directory structure
- use platform-agnostic Makefile
- display-manager: suppress minor lsof error
- firefox: add .db files that trust the freeipa cert
- ssh: add ~/.ssh/config
- xfce4: fix resize.desktop, and general update of all settings
- vimrc: add debian-related settings
- add policykit rules

* Sun Dec 29 2019 B Stack <bgstack15@gmail.com> - 0.1.19-0
- fix modify-ssh_config.sh to use current location of updateval script
- vimrc: add customization for Makefiles
- add volumeicon config
- add fluxbox config
- add bash prompt command

* Mon Dec 30 2019 B Stack <bgstack15@gmail.com> - 0.1.20-1
- add libreoffice calc
- fix bash prompt command invocation
- update fluxbox config

* Sun Jan 05 2020 B Stack <bgstack15@gmail.com> - 0.1.21-1
- update fluxbox config

* Wed Feb 12 2020 B Stack <bgstack15@gmail.com> - 0.1.22-1
- vimrc: add a few options
- fluxbox: update configs

* Wed Mar 11 2020 B. Stack <bgstack15@gmail.com> - 0.1.23-1
- fluxbox: update configs

* Tue Apr 14 2020 B. Stack <bgstack15@gmail.com> - 0.1.24-1
- fluxbox:
-  update for powerkit orand cbatticon
-  remove temporary workaround for debian wine error
- firefox: update a few configs and improve for post v75 days
- vimrc: add markdown customizations

* Thu Sep 03 2020 B. Stack <bgstack15@gmail.com> - 0.1.25-1
- add connman 
- add gtk

* Tue Feb 09 2021 B. Stack <bgstack15@gmail.com> - 0.1.26-1
- firefox: update userprefs (#3)
- vimrc: add a few settings (#6)
- xfce4: fix a few things
- add geeqie (#5)
- fix framework and modconf invocations (#4)

* Mon Jun 07 2021 B. Stack <bgstack15@gmail.com> - 0.1.27-1
- fluxbox: add items (#7)
- vimrc: add visual mode selected text search (#8)

* Thu Oct 21 2021 B. Stack <bgstack15@gmail.com> - 0.1.28-1
- fluxbox: update keys, startup
- policykit: add fprintd pkla (#9)

* Thu Dec 30 2021 B. Stack <bgstack15@gmail.com> - 0.1.29-1
- scite: improve sciteglobal.properties (#10)
- firefox: add clipboard pref to prefs.js (#11)
- puddletag: update prefs (#12)

* Mon Mar 21 2022 B. Stack <bgstack15@gmail.com> - 0.1.30-1
- fix some bgscripts invocations to use the libexec path (#14)
- add librewolf profile (#13)

* Tue Mar 29 2022 B. Stack <bgstack15@gmail.com> - 0.1.31-1
- xfce: improve many settings (#15)
- librewolf: improve profile deployment

* Wed Jul 13 2022 B. Stack <bgstack15@gmail.com> - 0.1.32-1
- vimrc: add timestamp functions (#16)
- librewolf: improve profiles, and firefox (#17)
- update nfs settings (#18)
- add gtk3 (#19)

* Wed Oct 05 2022 B. Stack <bgstack15@gmail.com> - 0.1.33-1
- git: update gitconfig
- browsers: update settings

* Wed Jan 25 2023 B. Stack <bgstack15@gmail.com> - 0.1.34-1
- browsers: update settings (#22)
- policykit: update fprintd rule for fprintd 1.94.2-2 (#23)
- vimrc: improve functions (#24, #25)

* Wed Jul 19 2023 B. Stack <bgstack15@gmail.com> - 0.1.35-1
- add jqrc (want#1)
- ssh: add vm2 ssh key (want#2)
- add geeqie atril plugin (want#3)
- browsers: update settings (#26)

* Fri Sep 08 2023 B. Stack <bgstack15@gmail.com> - 0.1.36-1
- connman: actually execute script

* Tue Dec 12 2023 B. Stack <bgstack15@gmail.com> - 0.1.37-1
- add dbus notification service file (want#5)
- fluxbox: update keys and startup to match stackrpms-thinkpad-p50s exactly (want#6)
- volumeicon: use pulseaudio config now (want#7)

* Fri Dec 15 2023 B. Stack <bgstack15@gmail.com> - 0.1.38-1
- add sshd_config.d conf (want#8)

* Sat Dec 30 2023 B. Stack <bgstack15@gmail.com> - 0.1.39-1
- sshd_config: remove AuthenticationMethods which is flakey
