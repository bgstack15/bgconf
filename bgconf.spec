# File: bgconf.spec
# Location: bgconf tarball
# Author: bgstack15
# Startdate: 2017
# Title: Rpm Spec for bgconf Package
# Purpose: Provide build instructions for Fedora rpm for package
# History:
#    2019-02-19 rewritten to use makefile
# Usage:
# Reference:
#    bgscripts.spec
# Improve:
# Documentation:
# Dependencies:

%global _python_bytecompile_errors_terminate_build 0

Summary: set of confs for standard deployments
Name:    bgconf
Version: 0.1.39
Release: 0
License: CC BY-SA 4.0
Group:   Applications/System
Source:  bgconf-%{version}.tgz
URL:     https://bgstack15.wordpress.com/
#Distribution:
#Vendor:
Packager:	B Stack <bgstack15@gmail.com>
Buildarch:	noarch
Requires:	/usr/bin/python3
# the lightdm conf requires bc
Requires:	bc
Requires:	bgscripts-core >= 1.4.0
%if 0%{?fedora} >= 34
Requires:   make
%endif
Obsoletes:  %{name} =< 0.1-17
%if 0%{?fedora} || 0%{?centos} >= 8
Recommends: xdg-themes-stackrpms
Suggests: fluxbox-themes-stackrpms
%endif

%description
Bgconf includes configs for standard applications in my PC deployments.
A special python script deploys the confs based on its own config file and the presence of the specified applications. See file /usr/share/bgconf/bgconf.conf.

%prep
%setup -q -c %{name}

%build

%install
# rpm install 2019-02-19
if test -e %{name}-%{version}/src ;
then
   pushd %{name}-%{version}/src
else
   pushd %{name}/src
fi
%make_install
popd
:

%clean
rm -rf %{buildroot}

%post
# rpm post 2017-10-14
# nothing to do here because the rpm can include the symlink for /usr/share/doc/bgconf
exit 0

%preun

%postun

%files
%{_bindir}/%{name}.py
%{_datadir}/%{name}
%{_datadir}/dbus-1/services/com.smith122.Notifications.service
%{_sysconfdir}/ssh/sshd_config.d/*
%attr(644, -, -) %{_sysconfdir}/profile.d/*
%attr(644, -, -) %{_datadir}/geeqie/applications/*
%if 0%{?el6}%{?el7}
%doc %attr(444, -, -) %{_docdir}/%{name}/*
%else
%doc %attr(444, -, -) %{_pkgdocdir}/*
%endif
%config %attr(644, -, -) /usr/share/bgconf/firefox.conf-example
%config %attr(644, -, -) /usr/share/bgconf/firefox.conf
%config %attr(644, -, -) /usr/share/bgconf/bgconf.conf-example
%config %attr(666, -, -) /usr/share/bgconf/bgconf.conf

%changelog
* Sat Dec 30 2023 B. Stack <bgstack15@gmail.com> - 0.1.39-1
- Updated content. See doc/README.md.
